# 20165301 2017-2018-2 《Java程序设计》第九周学习总结

## 教材学习内容总结
#### 第十三章：Java网络编程

- URL类
   - 通常包含三部分信息：协议、地址、资源
   - 协议必须是URL对象所在的Java虚拟机支持的协议
   - 地址必须是能连接的有效的IP地址或域名；
   - 资源可以是主机上的任何一个文件

- InetAddress类
    - 表示地址的方式：域名、IP地址
    - InetAddress类的方法：
    - ```getByName(String s)```将域名或IP地址传给该方法的参数s，获得InetAddress对象
    - ```public String getHostName()```获取InetAddress对象包含的域名
    - ```public String getHostAddress()```获取IP地址
    - ```getLocalHost()```获取含有本机域名和IP地址的对象

- UDP数据报
    - 基本通信模式：将数据打包，发往目的地；接受数据包，查看内容
    - 发送数据包：

```
DatagramPacket(byte data[],int length,InetAddtress address,int port)
```

```
DatagramPack(byte data[],int offset,int length,InetAddtress address,int port)
```

```
DatagramSocket mail_out=new DatagramSocket(); 
mail_out.send(data_pack);
```





## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/ctf20165301_JavaProgramming)





- statistics.sh脚本的运行结果截图
![](https://images2018.cnblogs.com/blog/1296455/201804/1296455-20180428202008220-2031015140.png)







## 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 11/11           |   1/1            | 4/4             |       |
| 第二周      | 286/297           |   2/3            | 6/10             |       |
| 第三周      | 722/1004          |   1/4            | 10/20             |       |
| 第四周      | 421/1425          |   1/5            | 10/30             |       |
| 第五周      | 829/2283          |   3/8            | 10/40             |      |
|第六周|943/3326|2/10|10/50|
|第七周|497/3823|1/11|10/60|
|第八周|703/4527|3/14|15/75|
|第九周|1569/6096|2/16|25/90|