# 20165301 2017-2018-2 《Java程序设计》第七周学习总结

## 教材学习内容总结
#### 第十一章：JDBC与MySQL数据库
- MySQL数据库管理系统
  
- 启动MySQL数据库服务器
   - 直接关闭MySQL所占的命令行窗口不能关闭MySQL数据库服务器，使用操作系统提供的“任务管理器”来关闭MySQL数据库管理器。
   - 修改任何用户密码```mysqladmin -u root -p password```
- MySQL客户端管理工具
- JDBC
   - JDBC为专门用来操作数据库的API
   - JDBC操作不同的数据库仅仅是连接方式上的差异。
- 查询操作
   - 向数据库发送SQL查询语句
   ```
   try{ Statement sql=con.creatStatement();
   }
   cath(SQLException e ){}
   ```
   - 处理查询结果 ```ResultSet rs = sql.executeQuery("SELECT*FROM students");```
   - 关闭连接```con.close()```
- 顺序查询
   - 使用ResultSet对象一次只能看到一个数据行，使用next()方法移到下一数据行，最初的查询位置就是游标位置。
- 控制游标
   - 获得Statement对象```Statement stmt = con.createStatement(int type,int concurrency) ```
- 条件与排序查询
   - where子语句
     - 一般格式：```select 字段 from 表名 where 条件```
     - 排序：用order by 子语句对记录进行排序
     ```
     selete * from mess order by height
     selete * from mess where name like '%林' order by name
     ```
- 更新、添加与删除操作
    - Statement对象调用方法
    ```
    public int executeUpdate(String sqlStatement);
    ```
    - 更新：```update 表 set 字段 = 新值 where <条件子句>```
    - 添加：```insert into 表(字段列表) values (对应的具体的记录)```
    - 删除
    ```
    delete from 表名 where <条件子句>
    ```
- 使用预处理语句
    - 预处理语句的优点：减轻了数据库的负担，也提高了访问数据库的速度
    - 使用通配符：在sql对象执行前，必须调用相应的方法设置通配符“？”代表的具体值
- 通用调查
    - ResultSet对象调用getMetaData()方法返回一个ResultSetMetaData对象，然后调用getColumnCount()方法就可以返回结果集rs中列的数目，调用其他方法可以实现相应的功能。
- 事务
    - 事务及处理：应用程序保证事务中的SQL语句要么全部都执行，要么一个都不执行。
    - 事务处理步骤
       - 用setAutoCommit(booean b)方法关闭自动提交模式
       - 用commit方法处理事务
       - 用rollback()方法处理事务失败
- 连接SQL Server数据库
- 连接Derby数据库




## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/ctf20165301_JavaProgramming)
- git出现了问题，代码暂时没有成功上传，正在调试中。




（statistics.sh脚本的运行结果截图）
![](https://images2018.cnblogs.com/blog/1296455/201804/1296455-20180415180453806-1699520258.png)






## 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 11/11           |   1/1            | 4/4             |       |
| 第二周      | 286/297           |   2/3            | 6/10             |       |
| 第三周      | 722/1004          |   1/4            | 10/20             |       |
| 第四周      | 421/1425          |   1/5            | 10/30             |       |
| 第五周      | 829/2283          |   1/6            | 10/40             |      |
|第六周|943/3326|2/8|10/50|
|第七周|497/3823|1/9|10/60|