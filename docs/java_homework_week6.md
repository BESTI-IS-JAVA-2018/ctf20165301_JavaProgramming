# 20165301 2017-2018-2 《Java程序设计》第六周学习总结

## 教材学习内容总结
#### 第七章：常用实类
- String类
   - 构造String对象
     - 常量对象
     - String对象
     ```
      String s = new String("we are students");
      ```
      ```
      char a[]={'J','a','v'.'a'};
      String s = new String(a);
      ```
   - 字符串的并置：String对象可以用+进行并置运算，即首尾相连得到一个新的String对象
- String类常用方法
   - ```public int length()```
   - ```public boolean equals(String s)```
   - ```public bollean startsWith(String s)、public boolean endsWith(String s)```
   - ```public int compareTo(String s)```
   - ```public boolean contain(String s)```
   - ```public int indexOf (String s)和public int lastIndexOf(String s)```
   - ```public String sbstring(int startpoint)```
- 字符串与基本数据的相互转化
```
intx;
String s = "876";
x = Integer.parseInt(s);
```
- 对象的字符串表示：创建对象的类的名字@对象的引用的字符串的表示


#### 第十五章：泛型与集合框架
- 用```class```名称<泛型列表>声明一个泛型类，使用泛类型的时候必须用具体的类型，不能用基本数据类型。
- ```LinkedList<E>```以链表的形式储存泛型类的创建对象，由若干个节点组成。
- ```Stack<E>```泛型类创建一个堆栈对象，第一个放入该对堆栈的放在最底下，后续的放入的数据放在已有的数据顶上。
- ```HashMap<K,V>```泛型类创建散列映射，散列映射采用散列表结构储存数据。

- ```TreeMap<K,V>```类创建树映射，树映射的结点存储键，保证结点是按照结点中的键升序排列。




## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/ctf20165301_JavaProgramming)


代码提交过程截图：![](https://images2018.cnblogs.com/blog/1296455/201804/1296455-20180408143456724-1704501191.png)

代码量截图：![](https://images2018.cnblogs.com/blog/1296455/201804/1296455-20180408143526438-918883529.png)


（statistics.sh脚本的运行结果截图）
![](https://images2018.cnblogs.com/blog/1296455/201804/1296455-20180408143402051-391759747.png)






## 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 11/11           |   1/1            | 4/4             |       |
| 第二周      | 286/297           |   2/3            | 6/10             |       |
| 第三周      | 722/1004          |   1/4            | 10/20             |       |
| 第四周      | 421/1425          |   1/5            | 10/30             |       |
| 第五周      | 829/2283          |   1/6            | 10/40             |      |
|第六周|943/3326|2/8|10/50|